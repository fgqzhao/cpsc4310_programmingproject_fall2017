To run the MSB_Apriori.jar please follow the following steps:
1. Place MSB_Apriori.jar in a directory
2. Place TestDatabase.txt in the same directory as MSB_Apriori.jar
3. Use cmd or terminal navigate to the directory containing MSB_Apriori.jar
4. Use "java -jar MSB_Apriori.jar /[location of TestDatabase.txt]" command to execute the program
   Example: java -jar MSB_Apriori.jar /Users/Frank/Desktop/TestDatabase.txt
5. Program will start execute, output similar to the following should appear in the cmd or terminal window

Data configuration source file loaded:
/Users/Frank/Desktop/TestDatabase.txt
-------------------------------
Basic Apriori Algorithm Output:
[FlashDrive] - Support: 3
[Camera] - Support: 5
[Smartphone] - Support: 3
[Television] - Support: 1
[Battery] - Support: 6
[ChargingCable] - Support: 9
[Mouse] - Support: 1
[Speaker] - Support: 2
[FlashDrive, Camera] - Support: 3
[FlashDrive, Battery] - Support: 1
[FlashDrive, ChargingCable] - Support: 2
[Camera, Battery] - Support: 2
[Camera, ChargingCable] - Support: 3
[Camera, Mouse] - Support: 1
[Smartphone, Battery] - Support: 2
[Smartphone, ChargingCable] - Support: 3
[Television, Speaker] - Support: 1
[Battery, ChargingCable] - Support: 5
[ChargingCable, Mouse] - Support: 1
[ChargingCable, Speaker] - Support: 1
[FlashDrive, Camera, Battery] - Support: 1
[FlashDrive, Camera, ChargingCable] - Support: 2
[FlashDrive, Battery, ChargingCable] - Support: 1
[Camera, Battery, ChargingCable] - Support: 1
[Camera, ChargingCable, Mouse] - Support: 1
[Smartphone, Battery, ChargingCable] - Support: 2
[FlashDrive, Camera, Battery, ChargingCable] - Support: 1
-------------------------------
MSB_Apriori Algorithm Output:
[Camera]
[Smartphone]
[Television]
[Battery]
[ChargingCable]
[Speaker]
[FlashDrive, Camera]
[Camera, ChargingCable]
[Smartphone, Battery]
[Smartphone, ChargingCable]
[Television, Speaker]
[Battery, ChargingCable]
[Smartphone, Battery, ChargingCable]